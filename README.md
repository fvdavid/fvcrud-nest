

	GET /companies - get many companies.
	GET /companies/:id - get one company.
	POST /companies/bulk - create many companies.
	POST /companies - create one company.
	PATCH /companies/:id - update one company.
	PUT /companies/:id - replace one company.
	DELETE /companies/:id - delete one company.


	- Pagination

	GET /companies?select=userName&sort=userName,ASC&page=1&limit=3
	(http://localhost:3000/companies?select=userName&sort=userName,ASC&page=1&limit=3)

