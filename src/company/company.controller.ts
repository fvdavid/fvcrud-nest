import { Controller, Get } from '@nestjs/common';

import { Company } from './company.entity';
import { CompanyService } from './company.service';

import { Crud } from '@nestjsx/crud';

@Crud({
  model: {
    type: Company,
  },
})
@Controller('companies')
export class CompanyController {
  constructor(public service: CompanyService) {}
}