import { Module } from '@nestjs/common';

import { Company } from './company.entity';
import { CompanyService } from './company.service';
import { CompanyController } from './company.controller';

import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [
    TypeOrmModule.forFeature([Company]),
  ],
  providers: [CompanyService],
  controllers: [CompanyController]
})
export class CompanyModule {}
